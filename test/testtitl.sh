# Makes the test title screen

rm ../art/loadscr/test/titleus.raw
tric tr3psx ../art/loadscr/us/titleus.raw /tmp/titleus.bmp
convert /tmp/titleus.bmp -fill red -font Roboto-Bold -strokewidth 1 -pointsize 25 -stroke black -gravity northeast -annotate +30+20 "Test Build\n$(date +%d-%m-%Y)" -define bmp:subtype=ARGB1555 /tmp/testtitl.bmp
tric tr3psx /tmp/testtitl.bmp ../art/loadscr/test/titleus.raw
rm /tmp/testtitl.bmp /tmp/titleus.bmp

