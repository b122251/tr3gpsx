Tomb Raider: The Lost Artefact for Sony Playstation
===================================================
Test build 18-02-2022

Thank you for being willing to help me test my PS1 conversion of Tomb Raider:
The Lost Artefact. Given that this is a test build, and not the final version,
I request you don't share this build of the conversion around to other people.
This is the first time I ever allow beta testing of any of my projects, please
don't let it be the last.

Please play around with this build of the game as much as you like, and send
any feedback you have, any found bugs or crashes, anything of interest to me
at hi@b122251.org.

Known bugs
----------
There are a few known bugs still to be worked out of the game at this stage.
These currently being:
- The level "Sleeping with the Fishes" tends to freeze and crash when some
  objects are loaded.
- The level "Sleeping with the Fishes" suffers from some corrupted geometry
  caused by the PS1 format's limitations in rooms 14, 44 and 53.
- The levels "Sleeping with the Fishes", "It's a Madhouse!" and "Reunion" don't
  have save crystals placed in the levels yet.

Once again thank you for participating, and for not sharing this test build
before the game is ready. I hope you'll enjoy your experience testing this
port, and that this will ensure this conversion will be the best it can be.

God bless,
b122251
