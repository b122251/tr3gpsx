# Shell script that packs this project directory in the desired 7-zip file.
# Parameters:
# # $1 = Password to use

#!/bin/sh
mkdir ./pack
mkdir ./pack/lpaq1
mkdir ./pack/lpaq1/bin
mkdir ./pack/lpaq1/src
tar -cvf ./pack/tr3gpsx.tar art bin disc fonts info.txt levels manual pack.sh unpack.sh test tombpsx tools
sha256 -q ./pack/tr3gpsx.tar >> ./pack/tr3gpsx.tar.sha256
cd ./pack/.
cp ../tools/bin/lpaq1 ./lpaq1/bin/.
cp ../tools/src/lpaq1.cpp ./lpaq1/src/.
./lpaq1/bin/lpaq1 9 ./tr3gpsx.tar ./tr3gpsx.lpq
sha256 -q ./tr3gpsx.lpq >> ./tr3gpsx.lpq.sha256
7z a -m0=Copy -mhe=on "-p${1}" /tmp/tr3gpsx.7z tr3gpsx.lpq lpaq1 tr3gpsx.tar.sha256 tr3gpsx.lpq.sha256

