#!/bin/sh
mkdir ./bin
cc --static -O3 -s -o ./bin/tomb2pc -DPC_VERSION ./src/main.c -Wall
cc --static -O3 -s -o ./bin/tomb2psx -DPSX_VERSION ./src/main.c -Wall
cc --static -O3 -s -o ./bin/tomb3pc -DPC_VERSION -DTOMB3_VERSION ./src/main.c -Wall
cc --static -O3 -s -o ./bin/tomb3psx -DPSX_VERSION -DTOMB3_VERSION ./src/main.c -Wall

