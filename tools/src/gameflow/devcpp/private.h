/* THIS FILE WILL BE OVERWRITTEN BY DEV-C++ */
/* DO NOT EDIT ! */

#ifndef PRIVATE_H
#define PRIVATE_H

/* VERSION DEFINITIONS */
#define VER_STRING	"0.1.0.0"
#define VER_MAJOR	0
#define VER_MINOR	1
#define VER_RELEASE	0
#define VER_BUILD	0
#define COMPANY_NAME	""
#define FILE_VERSION	"0.1.0.0"
#define FILE_DESCRIPTION	""
#define INTERNAL_NAME	""
#define LEGAL_COPYRIGHT	""
#define LEGAL_TRADEMARKS	""
#define ORIGINAL_FILENAME	""
#define PRODUCT_NAME	""
#define PRODUCT_VERSION	""

#endif /*PRIVATE_H*/
