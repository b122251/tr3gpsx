#include "gameflow.h"
#include "gf_data.h"

int	num_sequences;


sint16 *GF_LevelSequences[100];



/***********************************************/
/************* PC VERSION OF SCRIPT ************/
/***********************************************/
#ifdef PC_VERSION
/***********************************************/
/************** FRONTEND SEQUENCE **************/
sint16	GF_frontend[MAX_SEQ_SIZE]={
//	GFE_PICTURE,		PIC_EIDOS,
//	GFE_LIST_START,
//		GFE_PLAYFMV,	FMV_CORE,
//		GFE_PLAYFMV,	FMV_ESCAPE,
//	GFE_LIST_END,
//	GFE_PLAYFMV,		FMV_WALLOLD,
	GFE_END_SEQ
};

/***********************************************/
/************** LEVEL SEQUENCES ****************/
sint16 GF_assaultcourse[]={
	GFE_END_SEQ
};

sint16 GF_wall[]={
	GFE_PLAYFMV,		FMV_WALLPRESENT,
	GFE_SETTRACK,		3,
	GFE_STARTLEVEL,		THEWALL,
	GFE_CUTSCENE,		CUT_CULTCAMP_1,
	GFE_LEVCOMPLETE,
	GFE_END_SEQ
};
sint16 GF_boat[]={
	GFE_SETTRACK,		4,
	GFE_SUNSET,
	GFE_STARTLEVEL,		THEBOAT,
	GFE_LEVCOMPLETE,
	GFE_END_SEQ
};
sint16 GF_venice[]={
	GFE_SETTRACK,		5,
	GFE_SUNSET,
	GFE_STARTLEVEL,		VENICE,
	GFE_LEVCOMPLETE,
	GFE_END_SEQ
};
sint16 GF_opera[]={
	GFE_SETTRACK,		6,
	GFE_STARTLEVEL,		OPERA,
	GFE_CUTSCENE,		CUT_CARGOHOLD_2,
	GFE_LEVCOMPLETE,
	GFE_END_SEQ
};
sint16 GF_rig[]={
	GFE_PLAYFMV,		FMV_RIG,
	GFE_SETTRACK,		7,
	GFE_STARTLEVEL,		RIG,
	GFE_CUTSCENE,		CUT_RIG_3,
	GFE_LEVCOMPLETE,
	GFE_END_SEQ
};
sint16 GF_unwater[]={
	GFE_SETTRACK,		8,
	GFE_STARTLEVEL,		UNWATER,
	GFE_LEVCOMPLETE,
	GFE_END_SEQ
};
sint16 GF_keel[]={
	GFE_PLAYFMV,		FMV_MINISUB,
	GFE_SETTRACK,		9,
	GFE_STARTLEVEL,		KEEL,
	GFE_LEVCOMPLETE,
	GFE_END_SEQ
};
sint16 GF_living[]={
	GFE_SETTRACK,		10,
	GFE_STARTLEVEL,		LIVING,
	GFE_LEVCOMPLETE,
	GFE_END_SEQ
};
sint16 GF_dock[]={
	GFE_SETTRACK,		11,
	GFE_STARTLEVEL,		DECK,
	GFE_LEVCOMPLETE,
	GFE_END_SEQ
};
sint16 GF_skidoo[]={
	GFE_PLAYFMV,		FMV_SEAPLANE,
	GFE_SETTRACK,		12,
	GFE_SUNSET,
	GFE_STARTLEVEL,		SKIDOO_L,
	GFE_LEVCOMPLETE,
	GFE_END_SEQ
};
sint16 GF_monastry[]={
	GFE_SETTRACK,		13,
	GFE_STARTLEVEL,		MONASTRY,
	GFE_LEVCOMPLETE,
	GFE_END_SEQ
};
sint16 GF_catacomb[]={
	GFE_SETTRACK,		14,
	GFE_STARTLEVEL,		CATACOMB,
	GFE_LEVCOMPLETE,
	GFE_END_SEQ
};
sint16 GF_icecave[]={
	GFE_SETTRACK,		15,
	GFE_STARTLEVEL,		ICECAVE,
	GFE_LEVCOMPLETE,
	GFE_END_SEQ
};
sint16 GF_emprtomb[]={
	GFE_PLAYFMV,		FMV_JEEP,
	GFE_SETTRACK,		16,
	GFE_STARTLEVEL,		EMPRTOMB,
	GFE_LEVCOMPLETE,
	GFE_END_SEQ
};
sint16 GF_floating[]={
	GFE_CUTSCENE,		CUT_RITUAL_4,
	GFE_SETTRACK,		17,
	GFE_SUNSET,
	GFE_STARTLEVEL,		FLOATING,
	GFE_LEVCOMPLETE,
	GFE_PLAYFMV,		FMV_ENDSEQ,
	GFE_END_SEQ
};
sint16 GF_house[]={
	GFE_SETTRACK,		18,
	GFE_SUNSET,
	GFE_STARTLEVEL,		HOUSE,
	GFE_LEVCOMPLETE,
	GFE_END_SEQ
};


sint16 GF_test[]={
	GFE_SETTRACK,		2,
	GFE_STARTLEVEL,		TEST,
	GFE_LEVCOMPLETE,
	GFE_END_SEQ
};

/***********************************************/
/************** DEMO SEQUENCES *****************/
sint16 GF_demo1[]={
	GFE_PLAYFMV,		FMV_WALLPRESENT,
	GFE_DEMOPLAY,		DEM_1,
	GFE_END_SEQ
};


sint16 *GF_frontendSequence=GF_frontend;
sint16 *GF_level_sequence_list[MAX_LEVELS]={
	GF_assaultcourse,
	GF_test,
	GF_wall,
	GF_boat,
	GF_venice,
	GF_opera,
	GF_rig,
	GF_unwater,
	GF_keel,
	GF_living,
	GF_dock,
	GF_skidoo,
	GF_monastry,
	GF_catacomb,
	GF_icecave,
	GF_emprtomb,
	GF_floating,
	GF_house
};
sint16 *GF_demo_sequence_list[MAX_DEMOS]={
	GF_demo1
};



/***********************************************/
/************* PSX VERSION OF SCRIPT ************/
/***********************************************/
#else
/***********************************************/
/************** FRONTEND SEQUENCE **************/
sint16	GF_frontend[MAX_SEQ_SIZE]={
//	GFE_PICTURE,		PIC_EIDOS,
//	GFE_PLAYFMV,		FMV_CORE,
//	GFE_PLAYFMV,		FMV_WALLOLD,
	GFE_END_SEQ
};

/***********************************************/
/************** LEVEL SEQUENCES ****************/
sint16 GF_assaultcourse[]={
//	GFE_PLAYFMV,		FMV_ASSAULT,
//	GFE_STARTLEVEL,		ASSAULT,
	GFE_END_SEQ
};

sint16 GF_wall[]={
	GFE_PLAYFMV,		FMV_WALLPRESENT,
	GFE_SETTRACK,		3,
	GFE_STARTLEVEL,		THEWALL,
	GFE_CUTSCENE,		CUT_CULTCAMP_1,
	GFE_LEVCOMPLETE,
	GFE_END_SEQ
};
sint16 GF_boat[]={
	GFE_SETTRACK,		4,
	GFE_SUNSET,
	GFE_STARTLEVEL,		THEBOAT,
	GFE_LEVCOMPLETE,
	GFE_END_SEQ
};
sint16 GF_venice[]={
	GFE_SETTRACK,		5,
	GFE_SUNSET,
	GFE_STARTLEVEL,		VENICE,
	GFE_LEVCOMPLETE,
	GFE_END_SEQ
};
sint16 GF_opera[]={
	GFE_SETTRACK,		6,
	GFE_STARTLEVEL,		OPERA,
	GFE_CUTSCENE,		CUT_CARGOHOLD_2,
	GFE_LEVCOMPLETE,
	GFE_END_SEQ
};
sint16 GF_rig[]={
	GFE_PLAYFMV,		FMV_RIG,
	GFE_SETTRACK,		7,
	GFE_STARTLEVEL,		RIG,
	GFE_CUTSCENE,		CUT_RIG_3,
	GFE_LEVCOMPLETE,
	GFE_END_SEQ
};
sint16 GF_unwater[]={
	GFE_SETTRACK,		8,
	GFE_STARTLEVEL,		UNWATER,
	GFE_LEVCOMPLETE,
	GFE_END_SEQ
};
sint16 GF_keel[]={
	GFE_PLAYFMV,		FMV_MINISUB,
	GFE_SETTRACK,		9,
	GFE_STARTLEVEL,		KEEL,
	GFE_LEVCOMPLETE,
	GFE_END_SEQ
};
sint16 GF_living[]={
	GFE_SETTRACK,		10,
	GFE_STARTLEVEL,		LIVING,
	GFE_LEVCOMPLETE,
	GFE_END_SEQ
};
sint16 GF_dock[]={
	GFE_SETTRACK,		11,
	GFE_STARTLEVEL,		DECK,
	GFE_LEVCOMPLETE,
	GFE_END_SEQ
};
sint16 GF_skidoo[]={
	GFE_PLAYFMV,		FMV_SEAPLANE,
	GFE_SETTRACK,		12,
	GFE_SUNSET,
	GFE_STARTLEVEL,		SKIDOO_L,
	GFE_LEVCOMPLETE,
	GFE_END_SEQ
};
sint16 GF_monastry[]={
	GFE_SETTRACK,		13,
	GFE_STARTLEVEL,		MONASTRY,
	GFE_LEVCOMPLETE,
	GFE_END_SEQ
};
sint16 GF_catacomb[]={
	GFE_SETTRACK,		14,
	GFE_STARTLEVEL,		CATACOMB,
	GFE_LEVCOMPLETE,
	GFE_END_SEQ
};
sint16 GF_icecave[]={
	GFE_SETTRACK,		15,
	GFE_STARTLEVEL,		ICECAVE,
	GFE_LEVCOMPLETE,
	GFE_END_SEQ
};
sint16 GF_emprtomb[]={
	GFE_PLAYFMV,		FMV_JEEP,
	GFE_SETTRACK,		16,
	GFE_STARTLEVEL,		EMPRTOMB,
	GFE_LEVCOMPLETE,
	GFE_END_SEQ
};
sint16 GF_floating[]={
	GFE_CUTSCENE,		CUT_RITUAL_4,
	GFE_SETTRACK,		17,
	GFE_SUNSET,
	GFE_STARTLEVEL,		FLOATING,
	GFE_LEVCOMPLETE,
	GFE_PLAYFMV,		FMV_ENDSEQ,
	GFE_END_SEQ
};
sint16 GF_house[]={
	GFE_SETTRACK,		18,
	GFE_SUNSET,
	GFE_STARTLEVEL,		HOUSE,
	GFE_LEVCOMPLETE,
	GFE_END_SEQ
};

sint16 GF_test[]={
	GFE_SETTRACK,		2,
	GFE_STARTLEVEL,		TEST,
	GFE_LEVCOMPLETE,
	GFE_END_SEQ
};

/***********************************************/
/************** DEMO SEQUENCES *****************/
sint16 GF_demo1[]={
	GFE_PLAYFMV,		FMV_WALLPRESENT,
	GFE_DEMOPLAY,		DEM_1,
	GFE_END_SEQ
};


sint16 *GF_frontendSequence=GF_frontend;
sint16 *GF_level_sequence_list[MAX_LEVELS]={
	GF_assaultcourse,
	GF_test,
	GF_wall,
	GF_boat,
	GF_venice,
	GF_opera,
	GF_rig,
	GF_unwater,
	GF_keel,
	GF_living,
	GF_dock,
	GF_skidoo,
	GF_monastry,
	GF_catacomb,
	GF_icecave,
	GF_emprtomb,
	GF_floating,
	GF_house
};
sint16 *GF_demo_sequence_list[MAX_DEMOS]={
	GF_demo1
};

#endif
