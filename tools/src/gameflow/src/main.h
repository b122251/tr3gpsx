#include <stdio.h>
#include <string.h>
#include "typedefs.h"

#include "gameflow.h"
#include "gf_data.h"
#include "gf_data.c"

#include "fnames.c"
#include "sequence.c"
#include "strings.c"

#include "script.c"
