#include "typedefs.h"
#include "gameflow.h"
#include "gf_data.h"

/*
SEQUENCE FOR TOMB II

	FRONTEND	- EidosPIC/CoreLogoFMV/EscapeFMV
	FMV - Intro - Great Wall Ancient Time

	TITLE PAGE	-

	FMV-1		- Great Wall Present Time
	LEVEL 1		- WALL 'The Great Wall'
	CUT-1		- Cult Camp / Great Wall
	LEVELSTATS

	LEVEL 2		- BOAT Venice Canals

	LEVEL 3		- VENICE  (Bartoli's House)
	LEVELSTATS

	LEVEL 4		- OPERA House, Venice
	CUT-2		- CargoHold SeaPlane - Venice
	LEVELSTATS

	FMV-2		- Rig/Ocean
	LEVEL 5		- RIG /Ship
	CUT-3		- Rig/Changing Area
	LEVELSTATS

	FMV-3		- Mini Sub - grabs onto sub until crash
	LEVEL 6		- UNWATER Underwater (Diving Pool/Rig)
	LEVELSTATS

	LEVEL 7		- KEEL Sunken Ship
	LEVELSTATS

	LEVEL 8		- LIVING Sunken Ship
	LEVELSTATS

	LEVEL 9		- DOCK Sunken Ship
	LEVELSTATS

	FMV-4		- SeaPlane Taking off & crash onto tibet mountain
	LEVEL 10		- SKIDOO
	LEVELSTATS

	LEVEL 11	- MONASTRY
	LEVELSTATS

	LEVEL 12	- CATACOMB
	LEVELSTATS

	LEVEL 13	- ICECAVE
	LEVELSTATS

	FMV-5		- Mountain Side Tibet, Jeep ride to Great Wall
	LEVEL 14	- EMPRTOMB - Emporer's Tomb
	LEVELSTATS

	CUT-4		- Cult ritual

	LEVEL 15	- FLOATING
	LEVELSTATS
	FMV-6		- End Sequence?

	LEVEL 16	- HOUSE
*/

char	*title_option_strings[]={
	"STARTGAME",
	"STARTSAVEDGAME",
	"STARTCINE",
	"STARTFMV",
	"STARTDEMO",
	"EXIT_TO_TITLE",
	"LEVELCOMPLETE",
	"EXITGAME",
	"EXIT_TO_OPTION",
	"TITLE_DESELECT"
};

#ifdef TOMB3_VERSION
char *inv_types[]={
	"PISTOLS",
	"SHOTGUN",
	"AUTOPISTOLS",
	"UZIS",
	"HARPOON",
	"M16",
	"ROCKET",
	"GRENADE",

	"PISTOLS_AMMO",		// Whoops
	"SHOTGUN_AMMO",
	"AUTOPISTOLS_AMMO",
	"UZI_AMMO",
	"HARPOON_AMMO",
	"M16_AMMO",
	"ROCKET_AMMO",
	"GRENADE_AMMO",
	"FLARES",

	"MEDI",
	"BIGMEDI",

	"PICKUP1",
	"PICKUP2",
	"PUZZLE1",
	"PUZZLE2",
	"PUZZLE3",
	"PUZZLE4",
	"KEY1",
	"KEY2",
	"KEY3",
	"KEY4",
	"CRYSTAL"
};
#else
char *inv_types[]={
	"PISTOLS",
	"SHOTGUN",
	"AUTOPISTOLS",
	"UZIS",
	"HARPOON",
	"M16",
	"ROCKET",

	"PISTOLS_AMMO",		// Whoops
	"SHOTGUN_AMMO",
	"AUTOPISTOLS_AMMO",
	"UZI_AMMO",
	"HARPOON_AMMO",
	"M16_AMMO",
	"ROCKET_AMMO",
	"FLARES",

	"MEDI",
	"BIGMEDI",

	"PICKUP1",
	"PICKUP2",
	"PUZZLE1",
	"PUZZLE2",
	"PUZZLE3",
	"PUZZLE4",
	"KEY1",
	"KEY2",
	"KEY3",
	"KEY4"
};
#endif

char	*onoff_strings[]={
	"ON",
	"OFF",
	"ON",
};

char	*enadis_strings[]={
	"ENABLED",
	"DISABLED",
	"ENABLED"
};

sint16	GF_valid_demos[MAX_DEMOS] = {
		0,	 //ASSAULT,
		0,	 //THEWALL,
		0,	 //THEBOAT,
   		0,	 //VENICE,
   		0,	 //OPERA,
   		0,	 //RIG,
   		0,	 //UNWATER,
   		0,	 //KEEL,
   		0,	 //LIVING,
   		0,	 //DOCK,
   		0,	 //SKIDOO_L,
   		0,	 //MONASTRY,
   		0,	 //CATACOMB,
   		0,	 //ICECAVE,
   		0,	 //EMPRTOMB,
   		0,	 //FLOATING,
		0	 //HOUSE
};



/***********************************************/



#ifdef PC_VERSION
sint16 level_music[MAX_LEVELS] = {
	0,  //gym
	57, // 1
	57, // 2
	57, // 3a
	57, // 3b
	59, // 4
	59, // 5
	59, // 6
	58, // 7a
	58, // 7b
	59, // 8a
	59, // 8b
	59, // 8c
	58, // 10a
	60, // 10b
	60, // 10c
	0,0,0,0, // cuts
	2,  // title
	0
};
#endif

char GF_secret_totals[MAX_LEVELS] = {
       		   0,	//GYM,
       		   3,	//LEVEL1,
       		   3,	//LEVEL2,
       		   5,	//LEVEL3A,
       		   3,	//LEVEL3B,
       		   4,	//LEVEL4,
       		   3,	//LEVEL5,
       		   3,	//LEVEL6,
       		   3,	//LEVEL7A,
       		   2,	//LEVEL7B,
       		   3,	//LEVEL8A,
       		   3,	//LEVEL8B,
       		   1,	//LEVEL8C,
       		   3,	//LEVEL10A,
       		   3,	//LEVEL10B,
       		   3	//LEVEL10C,
};




#ifdef PSX_VERSION
/********** START OF PSX SPECIFIC BITS *****************/
char	*GF_loadpicfilenames[MAX_LEVELS] = {
			"deldata\\gymload.raw",
			"deldata\\aztecloa.raw",
			"deldata\\aztecloa.raw",
			"deldata\\aztecloa.raw",
			"deldata\\aztecloa.raw",
			"deldata\\greekloa.raw",
			"deldata\\greekloa.raw",
			"deldata\\greekloa.raw",
			"deldata\\greekloa.raw",
			"deldata\\greekloa.raw",
			"deldata\\egyptloa.raw",
			"deldata\\egyptloa.raw",
			"deldata\\egyptloa.raw",
			"deldata\\atlanloa.raw",
			"deldata\\atlanloa.raw",
			"deldata\\atlanloa.raw",
			"deldata\\aztecloa.raw",		// cut1 aztec
			"deldata\\greekloa.raw",		// cut2 greek
			"deldata\\atlanloa.raw",		// cut3 atlantis
			"deldata\\atlanloa.raw",  		// cut4 atlantis
#ifdef NTSC

#if LANGUAGE == 0
			"deldata\\amertit.raw",  		// NTSC title
#else
			"deldata\\japtit.raw",          // JAP title
#endif

#else
			"deldata\\psxtit.raw",  		// title
#endif
			"!"   							// current
		};

FMV_INFO	GF_fmv_data[MAX_FMV]=	{0};
/********** END OF PSX SPECIFIC BITS *****************/
#endif



/* General Game Info Variables (No. of levels, etc) */
// All set to defaults
GAMEFLOW_INFO gameflow={
	EXIT_TO_TITLE,	//sint32	firstOption;
	-1,				//sint32	title_replace;
	EXIT_TO_TITLE,	//sint32	ondeath_demo_mode;
	0,				//sint32	ondeath_ingame;
	30*30,			//sint32	noinput_time;
	EXIT_TO_TITLE,	//sint32	on_demo_interrupt;
	EXIT_TO_TITLE,	//sint32	on_demo_end;
	0,0,0,0,0,0,0,0,0, //spare


	NUM_LEVELS,		//sint16	num_levels;
	NUM_PICS,		//sint16	num_picfiles;
	NUM_TITLE,		//sint16	num_titlefiles;
	NUM_FMV,		//sint16	num_fmvfiles;
	NUM_CUTS,		//sint16	num_cutfiles;
	NUM_DEMOS,		//sint16	num_demos;
	2,				//sint16	title_track;
	-1,				//sint16	singlelevel;
	0,				//spare
	0,0,0,0,0,		//spare
	0,0,0,0,0,		//spare
	0,0,0,0,0,		//spare

	0xa6,		// cypher code
	0,			// language	0=ENGLISH
	47,			// secret track number
	0,			// stats track number
	0,0,0,0,	//pads

	0,			//none of below for normal release version
	//uint16	demoversion:1;
	//uint16	title_disabled:1;
	//uint16	cheatmodecheck_disabled:1;
	//uint16	noinput_timeout:1;
	//uint16	loadsave_disabled:1;
	//uint16	screensizing_disabled:1;
	//uint16	lockout_optionring:1;
	//uint16	dozy_cheat_enabled:1;
	//uint16	cyphered_strings:1;
	//uint16	gym_enabled:1;
	//uint16	play_any_level:1;		// Requester on middle pages of passport
	//uint16	cheat_enable:1;			// 'C' key fly, get weapons cheat
	//uint16	securitytag:1;
	0,0,0			//spare

};


