#include "main.h"

void write_strings(sint16 number, char **strings, FILE *file);
void print_seq(sint16 *seq);
int	sizeof_seq(sint16 *seq);
void init_puzzleStrings(void);
void write_strings_file(void);
void read_strings_file(void);
void print_usage(void);

#define	WRITE_SEQUENCE(seq) \
	size = sizeof_seq(seq); \
	fwrite(seq, size, 1, file );


FILE *logfile;
uint32	script_version = 3;
int	info = 0;

#ifdef PC_VERSION
char input_fname[256]={"scripts\\pcfinal.txt"};
char output_fname[256]={"tombPC.dat"};
#else
char input_fname[256]={"scripts\\psxdemo1.txt"};
char output_fname[256]={"tombPSX.dat"};
#endif

char log_fname[256]={"gameflow.log"};
char stringin_fname[256]={"strings.txt"};
char stringout_fname[256]={"scripts\\PCstrOUT.txt"};

#if 0
#define WRITE_ALIGN	number = 0x7777; \
					fwrite(&number,sizeof(sint16),1,file);		// Alignment Checker
#else
#define WRITE_ALIGN
#endif
/***************************************************************
		M A I N
***************************************************************/
void main( int argc, char *argv[], char *env[] )
{
	FILE *file;
	sint16	number,size;
	int	i, fname_set=0;
	uint16 option,level;

	if (argc==1)
	{
		print_usage();
		return;
	}

	for ( i=1;i<argc;i++ )
	{
		if (*(argv[i])=='/' || *(argv[i])=='\\' || *(argv[i])=='-')
		{
			// OPTIONS
			if (*(argv[i]+1)=='i' || *(argv[i]+1)=='I')
				info = 1;
			else
			{
				printf("Invalid option '%s'\n",argv[i]);
				print_usage();
				return;
			}
		}
		else if (!fname_set)
		{
			strcpy(input_fname,argv[i]);
			fname_set = 1;
		}
		else if (fname_set==1)
		{
			strcpy(stringin_fname,argv[i]);
		}
		else
		{
			printf("ERROR: Too many parameters\n\n");
			print_usage();
			return;
		}
	}

	if (!fname_set)
	{
		print_usage();
		return;
	}
	build_ext(input_fname,"txt");
	strcpy(log_fname,input_fname);
	strcpy(stringout_fname,input_fname);
	build_ext(log_fname,"log");
	build_ext(stringout_fname,"str");

	logfile = fopen(log_fname,"w");
	if (!logfile)
	{
		printf("ERROR: Couldn't open log file '%s'\n",log_fname);
		return;
	}

	init_puzzleStrings();
	gameflow.firstOption = EXIT_TO_TITLE;
	gameflow.title_replace = -1;
	gameflow.ondeath_demo_mode = EXIT_TO_TITLE;
	gameflow.ondeath_ingame = 0;
	gameflow.noinput_time = 30*30;
	gameflow.on_demo_interrupt = EXIT_TO_TITLE;
	gameflow.on_demo_end = EXIT_TO_TITLE;
	gameflow.num_levels = 0;
	gameflow.num_picfiles = 0;
	gameflow.num_titlefiles = 0;
	gameflow.num_fmvfiles = 0;
	gameflow.num_cutfiles = 0;
	gameflow.num_demos = 0;
	gameflow.title_track = 2;
	gameflow.singlelevel = -1;

	gameflow.demoversion = 0;
	gameflow.title_disabled = 0;
	gameflow.cheatmodecheck_disabled = 0;
	gameflow.noinput_timeout = 0;
	gameflow.loadsave_disabled = 0;
	gameflow.screensizing_disabled = 0;
	gameflow.lockout_optionring = 0;
	gameflow.dozy_cheat_enabled = 0;
	gameflow.cyphered_strings = 0;
	gameflow.gym_enabled = 0;
	gameflow.play_any_level = 0;
	gameflow.cheat_enable = 0;
	gameflow.securitytag = 0;

	gameflow.cypher_code = 0;
	gameflow.language = 0;
	gameflow.secret_track = 2;
	gameflow.stats_track = 0;

	if (!ReadScript(input_fname,logfile))
	{
		fclose(logfile);
		return;
	}

	file = fopen(output_fname,"wb");
	if (!file)
	{
		printf("ERROR: Couldn't open '%s'\n",output_fname);
		fclose(logfile);
		return;
	}
	printf("Producing gameflow script for;-\n\n	%s.\n\n", game_description);
	fprintf(logfile,"Gameflow script produced for;-\n\n	%s.\n\n", game_description);

	printf("Date Produced: %s %s\n\n",__TIME__,__DATE__);
	fprintf(logfile,"Date Produced: %s %s\n\n",__TIME__,__DATE__);

	printf("Script Version: %d\n\n", script_version);
	fprintf(logfile,"Script Version: %d\n\n", script_version);
	fwrite(&script_version, sizeof(uint32), 1, file);


	fwrite(game_description,sizeof(char),256,file);

	// Write out GAMEFLOW_INFO structure data
	size = sizeof(GAMEFLOW_INFO);
	fwrite(&size,sizeof(sint16),1,file);
	fwrite(&gameflow, size, 1, file);

	fprintf(logfile,"sizeof GAMEFLOW_INFO = %d\n\n",sizeof(GAMEFLOW_INFO));

	fprintf(logfile,"Gameflow Options;-\n");
	fprintf(logfile,"Levels:				%d\n",gameflow.num_levels);
	fprintf(logfile,"Demos:				%d\n",gameflow.num_demos);
	fprintf(logfile,"FMV:				%d\n",gameflow.num_fmvfiles);
	fprintf(logfile,"Cutscenes:			%d\n",gameflow.num_cutfiles);
	fprintf(logfile,"Pictures:			%d\n",gameflow.num_picfiles);
	fprintf(logfile,"Title Track:		%d\n",gameflow.title_track);
	fprintf(logfile,"TitleFiles:			%d\n",gameflow.num_titlefiles);

	if (gameflow.cyphered_strings)
		fprintf(logfile,"cypher_code:		%d\n",gameflow.cypher_code);

//	if (gameflow.demoversion)
//	{
		fprintf(logfile,"\nDemo Options;-\n");
		option = (uint16)gameflow.firstOption;
		level = option&0xff;
		option >>=8;
		if (option!=EXIT_TO_TITLE)
			fprintf(logfile,"firstOption:				%s  %d\n",title_option_strings[option],level);

		if (gameflow.title_disabled)
			fprintf(logfile,"TITLE IS DISABLED\n");

		if (gameflow.title_replace>=0)
		{
			option = (uint16)gameflow.title_replace;
			level = option&0xff;
			option >>=8;
			fprintf(logfile,"TITLE REPLACED WITH:		%s %d\n",title_option_strings[option],level);
		}

		option = (uint16)gameflow.ondeath_demo_mode;
		level = option&0xff;
		option >>=8;
		fprintf(logfile,"ondeath demo_mode:			%s %d\n",title_option_strings[option],level);

		option = (uint16)gameflow.ondeath_ingame;
		level = option&0xff;
		option >>=8;
		fprintf(logfile,"ondeath_ingame:				%s %d\n",title_option_strings[option],level);

		option = (uint16)gameflow.on_demo_interrupt;
		level = option&0xff;
		option >>=8;
		fprintf(logfile,"on_demo_interrupt:			%s %d\n",title_option_strings[option],level);

		option = (uint16)gameflow.on_demo_end;
		level = option&0xff;
		option >>=8;
		fprintf(logfile,"on_demo_end:				%s %d\n",title_option_strings[option],level);

		fprintf(logfile,"noinput_timeout:			%s\n",onoff_strings[gameflow.noinput_timeout]);
		fprintf(logfile,"NoInput_time:				%d\n",gameflow.noinput_time);

		fprintf(logfile,"cheatmodecheck:				%s\n", enadis_strings[gameflow.cheatmodecheck_disabled]);
		fprintf(logfile,"loadsave:					%s\n", enadis_strings[gameflow.loadsave_disabled]);
		fprintf(logfile,"screensizing:				%s\n", enadis_strings[gameflow.screensizing_disabled]);
		fprintf(logfile,"lockout_optionring:			%s\n",onoff_strings[gameflow.lockout_optionring+1]);
		fprintf(logfile,"dozy_cheat:					%s\n",enadis_strings[gameflow.dozy_cheat_enabled+1]);
//	}

	WRITE_ALIGN;

	// LEVEL NAMES
//	fprintf(file,"LEVELNAMES");
	printf("Level Names\n");
	fprintf(logfile,"\nLevel Names\n");
	write_strings(gameflow.num_levels, LevelName_Strings, file);

	WRITE_ALIGN;

	// MISC FILES
//	fprintf(file,"PIC_FNAMES");
	printf("Picture FileNames\n");
	fprintf(logfile,"\nPicture FileNames\n");
	write_strings(gameflow.num_picfiles, GF_picfilenames, file);

	WRITE_ALIGN;

	// TITLE FILES
//	fprintf(file,"TIT_FNAMES");
	printf("Title FileNames\n");
	fprintf(logfile,"\nTitle FileNames\n");
	write_strings(gameflow.num_titlefiles, GF_titlefilenames, file);

	WRITE_ALIGN;

	// FMV FILES
//	fprintf(file,"FMV_FNAMES");
	printf("FMV FileNames\n");
	fprintf(logfile,"\nFMV FileNames\n");
	write_strings(gameflow.num_fmvfiles, GF_fmvfilenames, file);

	WRITE_ALIGN;

	// LEVEL FILENAMES
//	fprintf(file,"LEV_FNAMES");
	printf("Level FileNames\n");
	fprintf(logfile,"\nLevel FileNames\n");
	write_strings(gameflow.num_levels, GF_levelfilenames, file);

	// LOADPIC FILENAMES (PSX)

	WRITE_ALIGN;

	// CUTSCENE FILENAMES
//	fprintf(file,"CUT_FNAMES");
	printf("Cutscene FileNames\n");
	fprintf(logfile,"\nCutscene FileNames\n");
	write_strings(gameflow.num_cutfiles, GF_cutscenefilenames, file);

	// DEMO FILENAMES
//	fprintf(file,"DEM_FNAMES");
//	printf("Demo FileNames\n");
//	fprintf(logfile,"\nDemo FileNames\n");
//	write_strings(gameflow.num_demos, GF_demofilenames, file);


	WRITE_ALIGN;

	// WRITE OUT BUFFER OFFSETS FOR EACH SEQ
//	fprintf(file,"OFFSETS");
	size = 0;
	fwrite(&size,sizeof(sint16),1,file);
	size = sizeof_seq(GF_frontendSequence);
	fprintf(logfile,"\nFRONTEND SEQ (SIZE: %d bytes)\n", size);
	print_seq(GF_frontendSequence);
	fwrite(&size,sizeof(sint16),1,file);

	for( i=0;i<gameflow.num_levels;i++)
	{
		number = sizeof_seq(GF_level_sequence_list[i]);
		fprintf(logfile,"\nLEVEL%d SEQ (SIZE: %d bytes)\n", i,number);
		print_seq(GF_level_sequence_list[i]);
		size += number;
		fwrite(&size,sizeof(sint16),1,file);

	}
/*	for( i=0;i<gameflow.num_demos;i++)
	{
		number = sizeof_seq(GF_demo_sequence_list[i]);
		fprintf(logfile,"\nDEMO%d SEQ (SIZE: %d bytes)\n", i,number);
		print_seq(GF_demo_sequence_list[i]);
		size += number;
		fwrite(&size,sizeof(sint16),1,file);
	}
*/
	WRITE_ALIGN;
	// TOTAL SIZE OF ALL SEQUENCES
	printf("\nTotal Sequence Data Size= %d bytes\n",size);
	fprintf(logfile,"\nTotal Sequence Data Size= %d bytes",size);
	// FRONTEND SEQUENCE
//	fprintf(file,"FRONTSEQ");
	printf("Writing FrontEnd Sequence\n");
	fprintf(logfile,"\nWriting FrontEnd Sequence\n");
	WRITE_SEQUENCE(GF_frontendSequence);

	// LEVEL SEQUENCES
	printf("Writing Level Sequences\n");
	fprintf(logfile,"\nWriting Level Sequences\n");
	for (i=0;i<gameflow.num_levels;i++)
	{
//		fprintf(file,"LEVELSEQ%d",i);
		WRITE_SEQUENCE(GF_level_sequence_list[i]);
	}
	// DEMO SEQUENCES
/*	printf("Writing Demo Sequences\n");
	fprintf(logfile,"\nWriting Demo Sequences\n");
	for (i=0;i<gameflow.num_demos;i++)
	{
//		fprintf(file,"DEMOSEQ%d",i);
		WRITE_SEQUENCE(GF_demo_sequence_list[i]);
	}
*/
	WRITE_ALIGN;

	printf("Writing Demo Level Numbers\n");
	fprintf(logfile,"Writing Demo Level Numbers\nDemo Levels: ");
	if (gameflow.num_demos)
	{
		fwrite(&GF_valid_demos,sizeof(sint16),gameflow.num_demos,file);
		for (i=0;i<gameflow.num_demos;i++)
			fprintf(logfile,"%d ",GF_valid_demos[i]);
	}
	else
		fprintf(logfile,"None");
	fprintf(logfile,"\n");

#ifdef PSX_VERSION
	WRITE_ALIGN;

	if (gameflow.num_fmvfiles)
	{
		printf("Writing FMV start/end frames\n");
		fprintf(logfile,"Writing FMV start/end frames\n");
		fwrite(GF_fmv_data,sizeof(FMV_INFO),gameflow.num_fmvfiles,file);
		for (i=0;i<gameflow.num_fmvfiles;i++)
		{
			fprintf(logfile,"%2d: %-20s	%d	- %d\n",i+1,GF_fmvfilenames[i],GF_fmv_data[i].startframe,GF_fmv_data[i].endframe);
		}
		fprintf(logfile,"\n");
	}
#endif

	WRITE_ALIGN;

	// GAMETEXT STRINGS
	number = GT_NUM_GAMESTRINGS;
	fwrite(&number,sizeof(sint16),1,file);

//	fprintf(file,"GAME_STRINGS");
	printf("Game Strings\n");
	fprintf(logfile,"\nGame Strings\n");
	write_strings(GT_NUM_GAMESTRINGS, game_strings, file);

	WRITE_ALIGN;

#ifdef PC_VERSION
	printf("PC Strings\n");
	fprintf(logfile,"\nPC Strings\n");
	write_strings(PCSTR_NUM_STRINGS, pc_strings, file);
#else
	printf("PSX Strings\n");
	fprintf(logfile,"\nPSX Strings\n");
	write_strings(PSSTR_NUM_STRINGS, psx_strings, file);
#endif

	WRITE_ALIGN;

	// PUZZLE1 STRINGS
//	fprintf(file,"PUZZLE1_STRINGS");
	printf("Puzzle1 Strings\n");
	fprintf(logfile,"\nPuzzle1 Strings\n");
	write_strings(gameflow.num_levels, puzzle1_strings, file);

	WRITE_ALIGN;

	// PUZZLE2 STRINGS
//	fprintf(file,"PUZZLE2_STRINGS");
	printf("Puzzle2 Strings\n");
	fprintf(logfile,"\nPuzzle2 Strings\n");
	write_strings(gameflow.num_levels, puzzle2_strings, file);

	WRITE_ALIGN;

	// PUZZLE3 STRINGS
//	fprintf(file,"PUZZLE3_STRINGS");
	printf("Puzzle3 Strings\n");
	fprintf(logfile,"\nPuzzle3 Strings\n");
	write_strings(gameflow.num_levels, puzzle3_strings, file);

	WRITE_ALIGN;

	// PUZZLE4 STRINGS
//	fprintf(file,"PUZZLE4_STRINGS");
	printf("Puzzle4 Strings\n");
	fprintf(logfile,"\nPuzzle4 Strings\n");
	write_strings(gameflow.num_levels, puzzle4_strings, file);


/*
	WRITE_ALIGN;

	// SECRET1 STRINGS
//	fprintf(file,"SECRET1_STRINGS");
	printf("Secret1 Strings\n");
	fprintf(logfile,"\nSecret1 Strings\n");
	write_strings(gameflow.num_levels, secret1_strings, file);

	WRITE_ALIGN;

	// SECRET2 STRINGS
//	fprintf(file,"SECRET2_STRINGS");
	printf("Secret2 Strings\n");
	fprintf(logfile,"\nSecret2 Strings\n");
	write_strings(gameflow.num_levels, secret2_strings, file);

	WRITE_ALIGN;

	// SECRET3 STRINGS
//	fprintf(file,"SECRET3_STRINGS");
	printf("Secret3 Strings\n");
	fprintf(logfile,"\nSecret3 Strings\n");
	write_strings(gameflow.num_levels, secret3_strings, file);

	WRITE_ALIGN;

	// SECRET4 STRINGS
//	fprintf(file,"SECRET4_STRINGS");
	printf("Secret4 Strings\n");
	fprintf(logfile,"\nSecret4 Strings\n");
	write_strings(gameflow.num_levels, secret4_strings, file);

	WRITE_ALIGN;

	// SPECIAL1 STRINGS
//	fprintf(file,"SPECIAL1_STRINGS");
	printf("Special1 Strings\n");
	fprintf(logfile,"\nSpecial1 Strings\n");
	write_strings(gameflow.num_levels, special1_strings, file);

	WRITE_ALIGN;

	// SPECIAL2 STRINGS
//	fprintf(file,"SPECIAL2_STRINGS");
	printf("Special2 Strings\n");
	fprintf(logfile,"\nSpecial2 Strings\n");
	write_strings(gameflow.num_levels, special2_strings, file);
*/
	WRITE_ALIGN;

	// PICKUP1 STRINGS
//	fprintf(file,"PICKUP1_STRINGS");
	printf("Pickup1 Strings\n");
	fprintf(logfile,"\nPickup1 Strings\n");
	write_strings(gameflow.num_levels, pickup1_strings, file);

	WRITE_ALIGN;

	// PICKUP2 STRINGS
//	fprintf(file,"PICKUP2_STRINGS");
	printf("Pickup2 Strings\n");
	fprintf(logfile,"\nPickup2 Strings\n");
	write_strings(gameflow.num_levels, pickup2_strings, file);

	WRITE_ALIGN;

	// KEY1 STRINGS
//	fprintf(file,"KEY1_STRINGS");
	printf("Key1 Strings\n");
	fprintf(logfile,"\nKey1 Strings\n");
	write_strings(gameflow.num_levels, key1_strings, file);

	WRITE_ALIGN;

	// KEY2 STRINGS
//	fprintf(file,"KEY2_STRINGS");
	printf("Key2 Strings\n");
	fprintf(logfile,"\nKey2 Strings\n");
	write_strings(gameflow.num_levels, key2_strings, file);

	WRITE_ALIGN;

	// KEY3 STRINGS
//	fprintf(file,"KEY3_STRINGS");
	printf("Key3 Strings\n");
	fprintf(logfile,"\nKey3 Strings\n");
	write_strings(gameflow.num_levels, key3_strings, file);

	WRITE_ALIGN;

	// KEY4 STRINGS
//	fprintf(file,"KEY4_STRINGS");
	printf("Key4 Strings\n");
	fprintf(logfile,"\nKey4 Strings\n");
	write_strings(gameflow.num_levels, key4_strings, file);

	WRITE_ALIGN;

	// FMV INFO (PSX)

	fclose(logfile);
	fclose(file);

	write_strings_file();

	printf("Lockout = %d", gameflow.lockout_optionring);
}






/***************************************************************
		S I Z E O F _ S E Q
***************************************************************/
int	sizeof_seq(sint16 *seq)
{
	int i;

	if (!seq)
	{
		printf("ERROR: No sequence!!!\n\n");
		fprintf(logfile,"ERROR: No sequence!!!\n\n");
		return(0);
	}
	for(i=0;i<MAX_SEQ_SIZE;i++)
	{
		switch(*(seq+i))
		{
			case GFE_PICTURE:
			case GFE_PLAYFMV:
			case GFE_STARTLEVEL:
			case GFE_CUTSCENE:
			case GFE_CUTANGLE:
			case GFE_DEMOPLAY:
			case GFE_JUMPTO_SEQ:
			case GFE_SETTRACK:
			case GFE_LOADINGPIC:
			case GFE_NOFLOOR:
			case GFE_ADD2INV:
			case GFE_STARTANIM:
			case GFE_NUMSECRETS:
				i++;
				break;
			case GFE_LIST_START:
			case GFE_LIST_END:
			case GFE_LEVCOMPLETE:
			case GFE_GAMECOMPLETE:
			case GFE_SUNSET:
			case GFE_DEADLY_WATER:
			case GFE_REMOVE_WEAPONS:
			case GFE_REMOVE_AMMO:
			case GFE_KILL2COMPLETE:
				break;
			case GFE_END_SEQ:
				return((i+1)*2);
			default:
				printf("ERROR: Unknown sequence command (%d)\n", *(seq+i+1));
				fprintf(logfile,"ERROR: Unknown sequence command (%d)\n", *(seq+i+1));
				return(0);
		}
	}
	return((i+1)*2);
}

/***************************************************************
		P R I N T _ S E Q
***************************************************************/
void print_seq(sint16 *seq)
{
	int	i,j;
	char buf[256];


	if (!seq)
	{
		printf("ERROR: No sequence!!!\n\n");
		fprintf(logfile,"ERROR: No sequence!!!\n\n");
		return;
	}


	for(i=0;i<MAX_SEQ_SIZE;i++)
	{
		switch(*(seq+i))
		{
			case GFE_PICTURE:
				if (gameflow.cyphered_strings)
				{
					strncpy(buf,GF_picfilenames[*(seq+i+1)],255);
					for (j=0;j<(int)strlen(buf);j++)
						buf[j] ^= gameflow.cypher_code;
					fprintf(logfile,"	PICTURE: %s\n", buf);
				}
				else
					fprintf(logfile,"	PICTURE: %s\n", GF_picfilenames[*(seq+i+1)]);
				i++;
				break;
			case GFE_LIST_START:
				fprintf(logfile,"	LIST START>>>\n");
				break;
			case GFE_LIST_END:
				fprintf(logfile,"	LIST END <<<<\n");
				break;
			case GFE_PLAYFMV:
				if (gameflow.cyphered_strings)
				{
					strncpy(buf,GF_fmvfilenames[*(seq+i+1)],255);
					for (j=0;j<(int)strlen(buf);j++)
						buf[j] ^= gameflow.cypher_code;
					fprintf(logfile,"	FMV: %s\n", buf);
				}
				else
					fprintf(logfile,"	FMV: %s\n", GF_fmvfilenames[*(seq+i+1)]);
				i++;
				break;
			case GFE_STARTLEVEL:
				if (gameflow.cyphered_strings)
				{
					strncpy(buf,LevelName_Strings[*(seq+i+1)],255);
					for (j=0;j<(int)strlen(buf);j++)
						buf[j] ^= gameflow.cypher_code;
					fprintf(logfile,"	STARTLEVEL: %s ", buf);
					strncpy(buf,GF_levelfilenames[*(seq+i+1)],255);
					for (j=0;j<(int)strlen(buf);j++)
						buf[j] ^= gameflow.cypher_code;
					fprintf(logfile,"'%s'\n", buf);
				}
				else
					fprintf(logfile,"	STARTLEVEL: %s '%s'\n", LevelName_Strings[*(seq+i+1)], GF_levelfilenames[*(seq+i+1)]);
				i++;
				break;
			case GFE_CUTSCENE:
				if (gameflow.cyphered_strings)
				{
					strncpy(buf,GF_cutscenefilenames[*(seq+i+1)],255);
					for (j=0;j<(int)strlen(buf);j++)
						buf[j] ^= gameflow.cypher_code;
					fprintf(logfile,"	CUTSCENE: %s\n", buf);
				}
				else
					fprintf(logfile,"	CUTSCENE: %s\n", GF_cutscenefilenames[*(seq+i+1)]);
				i++;
				break;
			case GFE_CUTANGLE:
				fprintf(logfile,"	CUT ANGLE: 0x%x\n",*(seq+i+1));
				i++;
				break;
			case GFE_LEVCOMPLETE:
				fprintf(logfile,"	LEVCOMPLETE\n");
				break;
			case GFE_GAMECOMPLETE:
				fprintf(logfile,"	GAMECOMPLETE\n");
				break;
			case GFE_DEMOPLAY:
				if (gameflow.cyphered_strings)
				{
					strncpy(buf,GF_levelfilenames[*(seq+i+1)],255);
					for (j=0;j<(int)strlen(buf);j++)
						buf[j] ^= gameflow.cypher_code;
					fprintf(logfile,"	DEMOPLAY: %s\n", buf);
				}
				else
					fprintf(logfile,"	DEMOPLAY: %s\n", GF_levelfilenames[*(seq+i+1)]);
				i++;
				break;
			case GFE_JUMPTO_SEQ:
				fprintf(logfile,"	JUMPTOSEQ: %d\n", *(seq+i+1));
				i++;
				break;
			case GFE_END_SEQ:
				fprintf(logfile,"	ENDSEQUENCE\n");
				return;
			case GFE_SETTRACK:
				fprintf(logfile,"	SETTRACK: %d\n", *(seq+i+1));
				i++;
				break;
			case GFE_SUNSET:
				fprintf(logfile,"	SUNSET ENABLED\n");
				break;
			case GFE_LOADINGPIC:
				if (gameflow.cyphered_strings)
				{
					strncpy(buf,GF_picfilenames[*(seq+i+1)],255);
					for (j=0;j<(int)strlen(buf);j++)
						buf[j] ^= gameflow.cypher_code;
					fprintf(logfile,"	LOADING PIC: %s\n", buf);
				}
				else
					fprintf(logfile,"	LOADING PIC: %s\n", GF_picfilenames[*(seq+i+1)]);
				i++;
				break;
			case GFE_DEADLY_WATER:
				fprintf(logfile,"	DEADLY WATER\n");
				break;
			case GFE_NOFLOOR:
				fprintf(logfile,"	NO FLOOR: %d\n",*(seq+i+1));
				i++;
				break;
			case GFE_STARTANIM:
				fprintf(logfile,"	LARA START ANIM: %d\n",*(seq+i+1));
				i++;
				break;
			case GFE_NUMSECRETS:
				fprintf(logfile,"	NUM SECRETS: %d\n",*(seq+i+1));
				i++;
				break;
			case GFE_ADD2INV:
				if (*(seq+i+1)<1000)
					fprintf(logfile,"	ADD TO INVENTORY: %s\n",inv_types[*(seq+i+1)]);
				else
					fprintf(logfile,"	START INVENTORY: %s\n",inv_types[*(seq+i+1)-1000]);
				i++;
				break;
			case GFE_REMOVE_WEAPONS:
				fprintf(logfile,"	REMOVE_WEAPONS\n");
				break;
			case GFE_REMOVE_AMMO:
				fprintf(logfile,"	REMOVE_AMMO\n");
				break;
			case GFE_KILL2COMPLETE:
				fprintf(logfile,"	KILL TO COMPLETE\n");
				break;
			default:
				printf("ERROR: Unknown sequence command (%d)\n", *(seq+i+1));
				fprintf(logfile,"ERROR: Unknown sequence command (%d)\n", *(seq+i+1));
				return;
		}
	}
}

/***************************************************************
		W R I T E _ S T R I N G S
***************************************************************/
void write_strings(sint16 number, char **strings, FILE *file)
{
	int	i,j;
   	sint16 size=0;
	char	*strptr;

	// write offsets for strings in this buffer
   	for (i=0;i<number;i++)
   	{
		fprintf(logfile,"%d:	%d		%s\n", i,size,strings[i]);
   		fwrite(&size,sizeof(sint16),1,file);
   		size += strlen(*(strings+i))+1;
   	}
   	fwrite(&size,sizeof(sint16),1,file);
	fprintf(logfile,"Strings = %d\n", number );
	fprintf(logfile,"Buffer size = %d\n", size );
	if (gameflow.cyphered_strings)
	{
		// encrypt text to deter casual meddling
   		for (i=0;i<number;i++)
	   	{
   			size = strlen(*(strings+i));
			strptr = *(strings+i);
			for (j=0;j<size;j++)
				strptr[j] ^= gameflow.cypher_code;
	   	}
	   	size = gameflow.cypher_code;
	}
	else
		size = 0;
	// write strings to file
   	for (i=0;i<number;i++)
   	{
   		fprintf(file, *(strings+i));
   		fwrite(&size,1,1,file);
   	}
}



void init_puzzleStrings(void)
{
	int i, ts=0;

	for( i=0;i<MAX_LEVELS;i++)
	{
		sprintf(tempstrings[ts], "P1");
		puzzle1_strings[i] = tempstrings[ts];
		ts++;
		sprintf(tempstrings[ts], "P2");
		puzzle2_strings[i] = tempstrings[ts];
		ts++;
		sprintf(tempstrings[ts], "P3");
		puzzle3_strings[i] = tempstrings[ts];
		ts++;
		sprintf(tempstrings[ts], "P4");
		puzzle4_strings[i] = tempstrings[ts];
		ts++;

		sprintf(tempstrings[ts], "S1");
		secret1_strings[i] = tempstrings[ts];
		ts++;
		sprintf(tempstrings[ts], "S2");
		secret2_strings[i] = tempstrings[ts];
		ts++;
		sprintf(tempstrings[ts], "S3");
		secret3_strings[i] = tempstrings[ts];
		ts++;
		sprintf(tempstrings[ts], "S4");
		secret4_strings[i] = tempstrings[ts];
		ts++;

		sprintf(tempstrings[ts], "S1");
		special1_strings[i] = tempstrings[ts];
		ts++;
		sprintf(tempstrings[ts], "S2");
		special2_strings[i] = tempstrings[ts];
		ts++;

		sprintf(tempstrings[ts], "P1");
		pickup1_strings[i] = tempstrings[ts];
		ts++;
		sprintf(tempstrings[ts], "P2");
		pickup2_strings[i] = tempstrings[ts];
		ts++;

		sprintf(tempstrings[ts], "K1");
		key1_strings[i] = tempstrings[ts];
		ts++;
		sprintf(tempstrings[ts], "K2");
		key2_strings[i] = tempstrings[ts];
		ts++;
		sprintf(tempstrings[ts], "K3");
		key3_strings[i] = tempstrings[ts];
		ts++;
		sprintf(tempstrings[ts], "K4");
		key4_strings[i] = tempstrings[ts];
		ts++;
	}
}

void write_strings_file(void)
{
	FILE *file;
	char buf[256];
	int i,j;

	if (!(file=fopen(stringout_fname,"w")))
	{
		printf("ERROR: Cannot open '%s'\n\n",stringout_fname);
		return;
	}
	// Level Names
	for(i=0;i<gameflow.num_levels;i++)
	{
		if (gameflow.cyphered_strings)
		{
			strcpy(buf,LevelName_Strings[i]);
			for(j=0;j<(int)strlen(buf);j++)
				buf[j] ^= gameflow.cypher_code;
			fprintf(file,"%s\n",buf);
		}
		else
			fprintf(file,"%s\n",LevelName_Strings[i]);
	}
	fprintf(file,"\n");

	// Game Strings
	for(i=0;i<GT_NUM_GAMESTRINGS;i++)
	{
		if (gameflow.cyphered_strings)
		{
			strcpy(buf,game_strings[i]);
			for(j=0;j<(int)strlen(buf);j++)
				buf[j] ^= gameflow.cypher_code;
			fprintf(file,"%s\n",buf);
		}
		else
			fprintf(file,"%s\n",game_strings[i]);
	}
	fprintf(file,"\n");
#ifdef PC_VERSION
	// PC Strings
	for(i=0;i<PCSTR_NUM_STRINGS;i++)
	{
		if (gameflow.cyphered_strings)
		{
			strcpy(buf,pc_strings[i]);
			for(j=0;j<(int)strlen(buf);j++)
				buf[j] ^= gameflow.cypher_code;
			fprintf(file,"%s\n",buf);
		}
		else
			fprintf(file,"%s\n",pc_strings[i]);
	}
	fprintf(file,"\n");
#else
	// PSX Strings
	for(i=0;i<PSSTR_NUM_STRINGS;i++)
	{
		if (gameflow.cyphered_strings)
		{
			strcpy(buf,psx_strings[i]);
			for(j=0;j<(int)strlen(buf);j++)
				buf[j] ^= gameflow.cypher_code;
			fprintf(file,"%s\n",buf);
		}
		else
			fprintf(file,"%s\n",psx_strings[i]);
	}
	fprintf(file,"\n");
#endif


	// Puzzle, Secret, Special, Pickup & key strings
	for(i=0;i<gameflow.num_levels;i++)
	{
		if (gameflow.cyphered_strings)
		{
			strcpy(buf,puzzle1_strings[i]);
			for(j=0;j<(int)strlen(buf);j++)
				buf[j] ^= gameflow.cypher_code;
			fprintf(file,"%s\n",buf);
		}
		else
			fprintf(file,"%s\n",puzzle1_strings[i]);
	}
	fprintf(file,"\n");
	for(i=0;i<gameflow.num_levels;i++)
	{
		if (gameflow.cyphered_strings)
		{
			strcpy(buf,puzzle2_strings[i]);
			for(j=0;j<(int)strlen(buf);j++)
				buf[j] ^= gameflow.cypher_code;
			fprintf(file,"%s\n",buf);
		}
		else
			fprintf(file,"%s\n",puzzle2_strings[i]);
	}
	fprintf(file,"\n");
	for(i=0;i<gameflow.num_levels;i++)
	{
		if (gameflow.cyphered_strings)
		{
			strcpy(buf,puzzle3_strings[i]);
			for(j=0;j<(int)strlen(buf);j++)
				buf[j] ^= gameflow.cypher_code;
			fprintf(file,"%s\n",buf);
		}
		else
			fprintf(file,"%s\n",puzzle3_strings[i]);
	}
	fprintf(file,"\n");
	for(i=0;i<gameflow.num_levels;i++)
	{
		if (gameflow.cyphered_strings)
		{
			strcpy(buf,puzzle4_strings[i]);
			for(j=0;j<(int)strlen(buf);j++)
				buf[j] ^= gameflow.cypher_code;
			fprintf(file,"%s\n",buf);
		}
		else
			fprintf(file,"%s\n",puzzle4_strings[i]);
	}
	fprintf(file,"\n");
/*
	for(i=0;i<gameflow.num_levels;i++)
	{
		if (gameflow.cyphered_strings)
		{
			strcpy(buf,secret1_strings[i]);
			for(j=0;j<(int)strlen(buf);j++)
				buf[j] ^= gameflow.cypher_code;
			fprintf(file,"%s\n",buf);
		}
		else
			fprintf(file,"%s\n",secret1_strings[i]);
	}
	fprintf(file,"\n");
	for(i=0;i<gameflow.num_levels;i++)
	{
		if (gameflow.cyphered_strings)
		{
			strcpy(buf,secret2_strings[i]);
			for(j=0;j<(int)strlen(buf);j++)
				buf[j] ^= gameflow.cypher_code;
			fprintf(file,"%s\n",buf);
		}
		else
			fprintf(file,"%s\n",secret2_strings[i]);
	}
	fprintf(file,"\n");
	for(i=0;i<gameflow.num_levels;i++)
	{
		if (gameflow.cyphered_strings)
		{
			strcpy(buf,secret3_strings[i]);
			for(j=0;j<(int)strlen(buf);j++)
				buf[j] ^= gameflow.cypher_code;
			fprintf(file,"%s\n",buf);
		}
		else
			fprintf(file,"%s\n",secret3_strings[i]);
	}
	fprintf(file,"\n");
	for(i=0;i<gameflow.num_levels;i++)
	{
		if (gameflow.cyphered_strings)
		{
			strcpy(buf,secret4_strings[i]);
			for(j=0;j<(int)strlen(buf);j++)
				buf[j] ^= gameflow.cypher_code;
			fprintf(file,"%s\n",buf);
		}
		else
			fprintf(file,"%s\n",secret4_strings[i]);
	}
	fprintf(file,"\n");

	for(i=0;i<gameflow.num_levels;i++)
	{
		if (gameflow.cyphered_strings)
		{
			strcpy(buf,special1_strings[i]);
			for(j=0;j<(int)strlen(buf);j++)
				buf[j] ^= gameflow.cypher_code;
			fprintf(file,"%s\n",buf);
		}
		else
			fprintf(file,"%s\n",special1_strings[i]);
	}
	fprintf(file,"\n");
	for(i=0;i<gameflow.num_levels;i++)
	{
		if (gameflow.cyphered_strings)
		{
			strcpy(buf,special2_strings[i]);
			for(j=0;j<(int)strlen(buf);j++)
				buf[j] ^= gameflow.cypher_code;
			fprintf(file,"%s\n",buf);
		}
		else
			fprintf(file,"%s\n",special2_strings[i]);
	}
	fprintf(file,"\n");
*/
	for(i=0;i<gameflow.num_levels;i++)
	{
		if (gameflow.cyphered_strings)
		{
			strcpy(buf,pickup1_strings[i]);
			for(j=0;j<(int)strlen(buf);j++)
				buf[j] ^= gameflow.cypher_code;
			fprintf(file,"%s\n",buf);
		}
		else
			fprintf(file,"%s\n",pickup1_strings[i]);
	}
	fprintf(file,"\n");
	for(i=0;i<gameflow.num_levels;i++)
	{
		if (gameflow.cyphered_strings)
		{
			strcpy(buf,pickup2_strings[i]);
			for(j=0;j<(int)strlen(buf);j++)
				buf[j] ^= gameflow.cypher_code;
			fprintf(file,"%s\n",buf);
		}
		else
			fprintf(file,"%s\n",pickup2_strings[i]);
	}
	fprintf(file,"\n");

	for(i=0;i<gameflow.num_levels;i++)
	{
		if (gameflow.cyphered_strings)
		{
			strcpy(buf,key1_strings[i]);
			for(j=0;j<(int)strlen(buf);j++)
				buf[j] ^= gameflow.cypher_code;
			fprintf(file,"%s\n",buf);
		}
		else
			fprintf(file,"%s\n",key1_strings[i]);
	}
	fprintf(file,"\n");
	for(i=0;i<gameflow.num_levels;i++)
	{
		if (gameflow.cyphered_strings)
		{
			strcpy(buf,key2_strings[i]);
			for(j=0;j<(int)strlen(buf);j++)
				buf[j] ^= gameflow.cypher_code;
			fprintf(file,"%s\n",buf);
		}
		else
			fprintf(file,"%s\n",key2_strings[i]);
	}
	fprintf(file,"\n");
	for(i=0;i<gameflow.num_levels;i++)
	{
		if (gameflow.cyphered_strings)
		{
			strcpy(buf,key3_strings[i]);
			for(j=0;j<(int)strlen(buf);j++)
				buf[j] ^= gameflow.cypher_code;
			fprintf(file,"%s\n",buf);
		}
		else
			fprintf(file,"%s\n",key3_strings[i]);
	}
	fprintf(file,"\n");
	for(i=0;i<gameflow.num_levels;i++)
	{
		if (gameflow.cyphered_strings)
		{
			strcpy(buf,key4_strings[i]);
			for(j=0;j<(int)strlen(buf);j++)
				buf[j] ^= gameflow.cypher_code;
			fprintf(file,"%s\n",buf);
		}
		else
			fprintf(file,"%s\n",key4_strings[i]);
	}
	fprintf(file,"\n");

	fclose(file);
}

void read_strings_file(void)
{

}

void	build_ext( char *fname, char *ext )
{
	while(!((*fname=='.' && *(fname+1)!='.') || *fname==0))
		fname++;

	*(fname++) = '.';
	*(fname++) = *(ext++);
	*(fname++) = *(ext++);
	*(fname++) = *(ext++);
	*(fname++) = 0;
}

void print_usage(void)
{
#ifdef PC_VERSION
	printf("\nGameFlow Script Converter (PC Version)\n\n");
	printf("Usage:	pcscript [/i] script.txt [strings.txt]\n");
	printf("				  /i = print info\n\n");
#else
	printf("\nGameFlow Script Converter (PSX Version)\n\n");
	printf("Usage:	psscript [/i] script.txt [strings.txt]\n");
	printf("				  /i = print info\n\n");
#endif
}

