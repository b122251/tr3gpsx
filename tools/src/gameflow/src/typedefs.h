//#ifndef _TYPEDEFS_H_INCLUDED
//#define _TYPEDEFS_H_INCLUDED

#define GAMEDEBUG
//#define RELOC
//
#include <stdint.h>

typedef uint8_t	uint8;
typedef int8_t		sint8;
typedef uint16_t	uint16;
typedef int16_t	sint16;
typedef uint32_t	uint32;
typedef  int32_t		sint32;

#ifdef bool
#undef	bool
typedef char			bool;
#endif

#ifndef NULL
#define NULL ((void*)0)
#endif

#ifndef FALSE
#define FALSE 0
#endif

#ifndef TRUE
#define TRUE 1
#endif

#ifdef GAMEDEBUG
#define DebugOut(args...) printf(args)
#else
#define DebugOut(args...)
#endif

typedef void (VOIDFUNCVOID)(void);
typedef void (VOIDFUNCSINT16)(sint16);

//#endif
