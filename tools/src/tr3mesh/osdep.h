/*
 * Library of OS-dependant functions used by TR2PCPSX.
 */
#ifndef OSDEP_H_
#define OSDEP_H_

#include "fixedint.h"

/* Function Declarations */
int truncateFile(char *filePath, INTU32 offset);

#endif
