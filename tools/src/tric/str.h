#ifndef STR_H_
#define STR_H_

int strtobmp(char *inFilePath, char *outFilePath);
int bmptostr(char *inFilePath, char *outFilePath);

#endif
