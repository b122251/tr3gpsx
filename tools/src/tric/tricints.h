/* Definition of fixed-size integers for TRIC */

/* 16-bit integer */
#ifndef INT16
#define INT16 short int
#endif

/* 32-bit integer */
#ifndef INT32
#define INT32 int
#endif
