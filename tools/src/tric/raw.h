#ifndef RAW_H_
#define RAW_H_

int rawtobmp(char *inFilePath, char *outFilePath, int type);
int bmptoraw(char *inFilePath, char *outFilePath, int type);

#endif
