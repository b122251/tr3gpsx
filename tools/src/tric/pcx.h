#ifndef PCX_H_
#define PCX_H_

int pcxtobmp(char *inFilePath, char *outFilePath);
int bmptopcx(char *inFilePath, char *outFilePath);

#endif
