#ifndef WAD_H_
#define WAD_H_

int unpackwad(char *inFilePath, char *outDirPath);
int packwad(char *inDirPath, char *outFilePath);

#endif
