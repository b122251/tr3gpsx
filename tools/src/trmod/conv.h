#ifndef TRMOD_CONV_H_
#define TRMOD_CONV_H_

/* File Inclusions */
#include "fixedint.h" /* Definition of fixed-size integers */

int16 convertLight(char *string, int type);

#endif
