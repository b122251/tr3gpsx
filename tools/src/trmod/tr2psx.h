#ifndef TRMOD_TR2PSX_H_
#define TRMOD_TR2PSX_H_

/* File Inclusions */
#include "structs.h" /* Definition of used structs */

/* Function Declarations */
void tr2psx_setup(struct level *level);

#endif
