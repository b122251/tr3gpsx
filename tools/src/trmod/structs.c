/*
 * Library that holds some variable relating to structs.h
 * Copyright (C) 2022 b122251
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not,see <http://www.gnu.org/licenses/>.
 */

/*
 * String equivalents of levparts
 */
const char *partnames[] =
{
	"NumVertices",
	"NumRectangles",
	"NumTriangles",
	"NumSprites",
	"NumDoors",
	"NumZSectors",
	"NumXSectors",
	"NumLights",
	"NumStaticMeshes",
	"AlternateRoom",
	"--",
	"NumTexTiles",
	"NumPalettes",
	"NumRooms",
	"OutsideRoomTable",
	"NumRoomMeshBoxes",
	"NumFloorData",
	"NumMeshData",
	"NumMeshPointers",
	"NumAnimations",
	"NumStateChanges",
	"NumAnimDispatches",
	"NumAnimCommands",
	"NumMeshTrees",
	"NumFrames",
	"NumMoveables",
	"NumStaticMeshes",
	"NumObjectTextures",
	"NumSpriteTextures",
	"NumSpriteSequences",
	"NumCameras",
	"NumSoundSources",
	"NumBoxes",
	"NumOverlaps",
	"Zones",
	"NumAnimatedTextures",
	"NumEntities",
	"NumRoomTextures",
	"NumCinematicFrames",
	"NumDemoData",
	"NumSoundDetails",
	"NumSamples",
	"NumSampleIndices",
	"Code Module",
	"--",
	"Vertex",
	"Rectangle",
	"Rectangle Texture",
	"Triangle",
	"Sprite",
	"Door",
	"Sector",
	"Roomlight",
	"Light",
	"Static Mesh",
	"--",
	"Texture Tile",
	"Palette",
	"Room Mesh Box",
	"FloorData",
	"MeshData",
	"Mesh Pointer",
	"Animation",
	"State Change",
	"Animation Dispatch",
	"Animation Command",
	"Meshtree",
	"Frame",
	"Moveable",
	"Static Mesh",
	"Object Texture",
	"Sprite Texure",
	"Sprite Sequence",
	"Camera",
	"Sound Source",
	"Box",
	"Overlap",
	"Animated Texture",
	"Entity",
	"Room Texture",
	"Cinematic Frame",
	"Demo Data",
	"Sound Details",
	"Sample",
	"Sample Index",
	"--",
	"--",
	"--",
	"--",
	"Skipped Bytes",
	"Skipped Bytes",
	"Skipped Bytes"
};
