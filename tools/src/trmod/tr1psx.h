#ifndef TRMOD_TR1PSX_H_
#define TRMOD_TR1PSX_H_

/* File Inclusions */
#include "fixedint.h" /* Definition of fixed-size integers */
#include "structs.h"  /* Definition of used structs */
#include "errors.h"   /* Definition of error codes */

/* Function Declarations */
void tr1psx_setup(struct level *level);

#endif
