#ifndef TR3PCPSX_VERSION_H_
#define TR3PCPSX_VERSION_H_

/* Program Name */
#ifndef PROGNAME
#define PROGNAME "tr3pcpsx"
#endif

/* Program Version */
#ifndef PROGVER
#define PROGVER " 1.0.0"
#endif

#endif
