//========================================================================
//========================================================================
//========================================================================
//
//	TOMB RAIDER III GOLD FULL SCRIPT
//
//	This script was not produced and is not supported or endorsed
//	by Eidos or Core Design.
//
//	Based in the research format by IceBerg.
//
//	Recreation by SuiKaze Raider.
//	Version: 1.0.0.0
//
//========================================================================
//========================================================================
//========================================================================



DESCRIPTION: Tomb Raider III Script. Final Release (c) Core Design Ltd 1998



//------------------------------------------------------------------------
//
//      O P T I O N S
//
//------------------------------------------------------------------------



OPTIONS:

   LANGUAGE:		ENGLISH
   PCNOINPUT_TIME:	9000
   PSXNOINPUT_TIME:	500
   CYPHER_CODE:		166
   SECRET_TRACK:	122
   STATS_TRACK:		14

END:



//------------------------------------------------------------------------
//
//      T I T L E
//
//------------------------------------------------------------------------



TITLE:

   GAME:	data\title.tr2		 // Menu Rings

   PCFILE:	pix\titleuk.bmp
   PCFILE:	pix\copyrus.bmp
   PCFILE:	data\titleUS.pcx
   PCFILE:	data\legalUS.pcx
   PCFILE:	data\titleJAP.pcx
   PCFILE:	data\legalJAP.pcx

   PSXFILE:	pixUK\titleuk.raw
   PSXFILE:	pixUK\legaluk.raw
   PSXFILE:	pixUS\titleUS.raw
   PSXFILE:	pixUS\legalUS.raw
   PSXFILE:	pixJAP\titleJAP.raw
   PSXFILE:	pixJAP\legalJAP.raw

   TRACK:	5	// Title

END:



//------------------------------------------------------------------------
//
//      F R O N T E N D
//
//------------------------------------------------------------------------



FRONTEND:

   FMV_START:	1	// $1
   FMV_END:	817	// $331
   PSXFMV:	fmv\logo.fmv
   PCFMV:	fmv\logo.rpl

END:



//------------------------------------------------------------------------
//
//      G Y M
//
//------------------------------------------------------------------------



GYM:		Lara's House		 // Level 0

   LOAD_PIC:	pix\house.bmp
   TRACK:	0
   GAME:	data\house.tr2
   PSXSTARTINV:	CRYSTAL

   KEY1:	Racetrack Key

END:



//------------------------------------------------------------------------
//
//      L E V E L S
//
//------------------------------------------------------------------------



LEVEL:		Highland Fling		 // Level 1

   TRACK:	36
   LOAD_PIC:	pix\highland.bmp
   GAME:	data\scotland.tr2
   COMPLETE:

   PUZZLE1:	Crowbar
   PUZZLE2:	Thistle Stone

END:

//-------------------------------------------------

LEVEL:		Willard's Lair		 // Level 2

   TRACK:	30
   LOAD_PIC:	pix\willard.bmp
   GAME:	data\willsden.tr2
   COMPLETE:

   PUZZLE1:	Crowbar
   KEY1:	Cairn Key

END:

//-------------------------------------------------

LEVEL:		Shakespeare Cliff	 // Level 3

   TRACK:	74
   LOAD_PIC:	pix\chunnel.bmp
   GAME:	data\chunnel.tr2
   COMPLETE:

   PUZZLE1:	Pump Access Disk
   KEY1:	Drill Activator Card

END:

//-------------------------------------------------

LEVEL:		Sleeping with the Fishes // Level 4

   TRACK:	27
   LOAD_PIC:	pix\undersea.bmp
   GAME:	data\undersea.tr2
   COMPLETE:

   PUZZLE1:	Circuit Bulb
   PUZZLE2:	Mutant Sample
   PUZZLE3:	Mutant Sample
   PUZZLE4:	Circuit Bulb
   PICKUP1:	The Hand of Rathmore

END:

//-------------------------------------------------

LEVEL:		It's a Madhouse!	 // Level 5

   TRACK:	34
   LOAD_PIC:	pix\zoo.bmp
   GAME:	data\zoo.tr2
   COMPLETE:

   PUZZLE1:	The Hand of Rathmore
   KEY1:	Zoo Key
   KEY4:	Aviary Key

END:

//-------------------------------------------------

LEVEL:		Reunion			 // Level 6

   TRACK:	26
   LOAD_PIC:	pix\slinc.bmp
   GAME:	data\slinc.tr2
   GAMECOMPLETE:

   PUZZLE1:	The Hand of Rathmore

END:



//========================================================================
//========================================================================
//========================================================================

PCGameStrings:	pc_strings.txt
PSXGameStrings:	psx_strings.txt

//========================================================================
//========================================================================
//========================================================================
