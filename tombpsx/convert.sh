######################################################################
# Tomb Raider : The Lost Artefact PS1 TOMBSPSX.DAT Conversion Script #
######################################################################

# Versions of the game
versions=("us" "uk" "de" "fr" "it" "sp" "jp" "jp2" "as" "as2")

# Makes a new out-directory
rm -rf ./out
mkdir ./out

for ver in "${versions[@]}"
do
	cd "./src/$ver"
	../../../tools/bin/gameflow tr3_gold.txt
	mv tombPSX.dat ../../out/$ver.dat
	../../../tools/bin/gameflow test.txt
	mv tombPSX.dat ../../out/${ver}test.dat
	cd ../..
done

