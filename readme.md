# Tomb Raider : The Lost Artifact (PSX)

## Introduction
This repository contains my entire project directory for my PS1-conversion of Tomb Raider : The Lost Artefact. I have posted some updates on this project in the past on [this thread](https://www.tombraiderforums.com/showthread.php?t=226112). It was met with some excitement from many people online. However, I've decided to leave the "Tomb Raider fan community" and the world of video gaming in general, and therefore, it's unlikely that I will ever finish this project myself. For a detailed explanation of why I'm leaving, you can read my thread [here](https://www.tombraiderforums.com/showthread.php?t=228095). So, as a farewell gift, I've decided to publish the entire project here publicly, so others might finish it.

## Where to start
From the start, I tried to lay out this project as clearly as possible, so that I might be able to easily return to it later. To get your bearings, I suggest reading the info.txt-file in the root of the repository as a place to start. To use this project, I recommend running an x86_64 version of Linux, any distro will do (I used TinyCore Linux). It is also possible to run all of this on any other Unix-like system (I started this project on OpenBSD), but you'd have to recompile all tools for that.

## State of the project
At the time of writing this, the project is about 95% done. All levels except Sleeping with the Fishes are finished. The binaries are finished as well. There are some TOMBPSX-files to be done (especially the Japanese translation), and some legal/title screens are still missing.

## Final requests
As my online presence is now ending, and this project is my baby, I would like you guys to treat it well. I invite everyone who is capable and willing to work on it, but I have some requests:
- Don't release it until it's actually finished. Be perfectionistic, and make it as spectacular as a PS1-port of The Lost Artefact deserves to be.
- Don't spin off into a million forks. Try to organise around one repo.
- When it's released, don't scrub my or anyone else's name from the credits. If this project is to be done collaboratively, give everyone who worked on it the honour they merited.

Good luck, and God bless,
b122251
