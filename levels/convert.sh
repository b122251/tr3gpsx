#!/bin/sh

###############################################################
# Tomb Raider : The Lost Artefact PS1 Level Conversion Script #
###############################################################

### VARIABLE DEFINITIONS ###

# Definition of levels and versions
levels="scotland,willsden,chunnel,undersea,zoo,slinc"
versions="us,ukrev,derev,frrev,itrev,sprev,jp,jp2,as,as2"

# Definition of extra conversion parameters per level
convparams="scotland:-wr -mo;willsden:-nm;chunnel:-nm;undersea:-wc==50A3F8 -nm;zoo:-nm;slinc:-nm"

# Definition of SFX-samples to be removed for each level
nosfx="scotland:;willsden:117,334,081,082,237,218,047,217,084,083,329,269,021,022,023,024,025,061;chunnel:021,022,023,024,025,031,047,052,078,095,166,208,209,210,223,334,335,341,342;undersea:021,022,023,024,025,068,072,117,144,218,281;zoo:043,083,084,165,220;slinc:"

# Length of the shell command buffer
shellbuffer=8000


### SETUP ###

# Replaces the comma's with newlines
versions="$(echo "${versions}" |
            tr ',' '\n')"
levels="$(echo "${levels}" |
          tr ',' '\n')"

# Copies work files into /tmp
prjdir="${PWD}"
rm -rf ./out
rm -rf /tmp/levels
cp -r ../levels /tmp/.
cp ../tools/bin/* /tmp/levels/.
cd /tmp/levels/.
mv ./in/* .

# Makes output directories
mkdir ./out
for ver in ${versions}; do
	mkdir "./out/${ver}"
done


### SFX ###

# Makes the sfx-files for each level and version
mkdir "./outsfx"
for version in ${versions}; do
	mkdir "./outsfx/${version}"
done
for level in ${levels}; do
	mkdir ./tmpsfx
	cp ./sfx/common/* ./tmpsfx/.
	remsfx="$(echo "${nosfx}"  |
	          tr   ';' '\n'    |
	          grep "${level}:" |
	          sed "s|.*:||g"   |
	          tr ',' '\n')"
	for sample in ${remsfx}; do
		rm "./tmpsfx/${sample}.wav"
		cp "./sfx/empty.wav" "./tmpsfx/${sample}.wav"
	done
	for version in ${versions}; do
		cp -r ./tmpsfx ./tmpsfx2
		cp -r ./sfx/${version}/* ./tmpsfx2/.
		./trsfx "./tmpsfx2" "./outsfx/${version}/${level}.sfx"
		rm -rf "./tmpsfx2"
	done
	rm -rf "./tmpsfx"
done


### LEVEL CUSTOMISATION ###

# Replaces the texture data in all the levels from the bitmap image
for level in ${levels}; do
	./3texrep "${level}.tr2" "./textures/${level}.bmp"
done

# Adds the object textures for controller and save crystal
./texmod scotland.tr2 a 12 255 31 255 255 255 31 1 240 1 0 255 255 0 0 0 0 0002
./texmod scotland.tr2 a 12 255 31 255 255 1 0 255 255 255 31 1 240 0 0 0 0 0002
./texmod scotland.tr2 a 12 1 32 1 240 255 47 1 240 255 47 255 255 1 32 255 255 0000
./texmod scotland.tr2 a 12 1 48 1 240 255 63 1 240 255 63 255 255 1 48 255 255 0000
./texmod scotland.tr2 a 12 1 64 1 240 255 79 1 240 255 79 255 255 1 64 255 255 0000
./texmod scotland.tr2 a 12 1 80 1 240 255 95 1 240 255 95 255 255 1 80 255 255 0000
./texmod scotland.tr2 a 12 1 96 1 240 255 98 1 240 255 98 255 242 0 0 0 0 0000
./texmod scotland.tr2 a 12 1 99 1 240 255 101 1 240 255 101 255 242 0 0 0 0 0000
./texmod scotland.tr2 a 12 1 102 1 240 255 104 1 240 255 104 255 242 0 0 0 0 0000
./texmod scotland.tr2 a 12 1 105 1 240 255 107 1 240 255 107 255 242 0 0 0 0 0000
./texmod scotland.tr2 a 12 1 108 1 240 255 110 1 240 255 110 255 242 0 0 0 0 0000
./texmod scotland.tr2 a 12 1 111 1 240 255 113 1 240 255 113 255 242 0 0 0 0 0000
./texmod scotland.tr2 a 12 1 114 1 240 255 116 1 240 255 116 255 242 0 0 0 0 0000
./texmod scotland.tr2 a 12 1 117 1 240 255 119 1 240 255 119 255 242 0 0 0 0 0000
./texmod willsden.tr2 a 14 255 31 255 255 255 31 1 240 1 0 255 255 0 0 0 0 0002
./texmod willsden.tr2 a 14 255 31 255 255 1 0 255 255 255 31 1 240 0 0 0 0 0002
./texmod willsden.tr2 a 14 1 32 1 240 255 47 1 240 255 47 255 255 1 32 255 255 0000
./texmod willsden.tr2 a 14 1 48 1 240 255 63 1 240 255 63 255 255 1 48 255 255 0000
./texmod willsden.tr2 a 14 1 64 1 240 255 79 1 240 255 79 255 255 1 64 255 255 0000
./texmod willsden.tr2 a 14 1 80 1 240 255 95 1 240 255 95 255 255 1 80 255 255 0000
./texmod willsden.tr2 a 14 1 96 1 240 255 98 1 240 255 98 255 242 0 0 0 0 0000
./texmod willsden.tr2 a 14 1 99 1 240 255 101 1 240 255 101 255 242 0 0 0 0 0000
./texmod willsden.tr2 a 14 1 102 1 240 255 104 1 240 255 104 255 242 0 0 0 0 0000
./texmod willsden.tr2 a 14 1 105 1 240 255 107 1 240 255 107 255 242 0 0 0 0 0000
./texmod willsden.tr2 a 14 1 108 1 240 255 110 1 240 255 110 255 242 0 0 0 0 0000
./texmod willsden.tr2 a 14 1 111 1 240 255 113 1 240 255 113 255 242 0 0 0 0 0000
./texmod willsden.tr2 a 14 1 114 1 240 255 116 1 240 255 116 255 242 0 0 0 0 0000
./texmod willsden.tr2 a 14 1 117 1 240 255 119 1 240 255 119 255 242 0 0 0 0 0000
./texmod chunnel.tr2 a 13 255 31 255 255 255 31 1 240 1 0 255 255 0 0 0 0 0002
./texmod chunnel.tr2 a 13 255 31 255 255 1 0 255 255 255 31 1 240 0 0 0 0 0002
./texmod chunnel.tr2 a 13 1 32 1 240 255 47 1 240 255 47 255 255 1 32 255 255 0000
./texmod chunnel.tr2 a 13 1 48 1 240 255 63 1 240 255 63 255 255 1 48 255 255 0000
./texmod chunnel.tr2 a 13 1 64 1 240 255 79 1 240 255 79 255 255 1 64 255 255 0000
./texmod chunnel.tr2 a 13 1 80 1 240 255 95 1 240 255 95 255 255 1 80 255 255 0000
./texmod chunnel.tr2 a 13 1 96 1 240 255 98 1 240 255 98 255 242 0 0 0 0 0000
./texmod chunnel.tr2 a 13 1 99 1 240 255 101 1 240 255 101 255 242 0 0 0 0 0000
./texmod chunnel.tr2 a 13 1 102 1 240 255 104 1 240 255 104 255 242 0 0 0 0 0000
./texmod chunnel.tr2 a 13 1 105 1 240 255 107 1 240 255 107 255 242 0 0 0 0 0000
./texmod chunnel.tr2 a 13 1 108 1 240 255 110 1 240 255 110 255 242 0 0 0 0 0000
./texmod chunnel.tr2 a 13 1 111 1 240 255 113 1 240 255 113 255 242 0 0 0 0 0000
./texmod chunnel.tr2 a 13 1 114 1 240 255 116 1 240 255 116 255 242 0 0 0 0 0000
./texmod chunnel.tr2 a 13 1 117 1 240 255 119 1 240 255 119 255 242 0 0 0 0 0000
./texmod undersea.tr2 a 12 255 31 255 255 255 31 1 240 1 0 255 255 0 0 0 0 0002
./texmod undersea.tr2 a 12 255 31 255 255 1 0 255 255 255 31 1 240 0 0 0 0 0002
./texmod undersea.tr2 a 12 1 32 1 240 255 47 1 240 255 47 255 255 1 32 255 255 0000
./texmod undersea.tr2 a 12 1 48 1 240 255 63 1 240 255 63 255 255 1 48 255 255 0000
./texmod undersea.tr2 a 12 1 64 1 240 255 79 1 240 255 79 255 255 1 64 255 255 0000
./texmod undersea.tr2 a 12 1 80 1 240 255 95 1 240 255 95 255 255 1 80 255 255 0000
./texmod undersea.tr2 a 12 1 96 1 240 255 98 1 240 255 98 255 242 0 0 0 0 0000
./texmod undersea.tr2 a 12 1 99 1 240 255 101 1 240 255 101 255 242 0 0 0 0 0000
./texmod undersea.tr2 a 12 1 102 1 240 255 104 1 240 255 104 255 242 0 0 0 0 0000
./texmod undersea.tr2 a 12 1 105 1 240 255 107 1 240 255 107 255 242 0 0 0 0 0000
./texmod undersea.tr2 a 12 1 108 1 240 255 110 1 240 255 110 255 242 0 0 0 0 0000
./texmod undersea.tr2 a 12 1 111 1 240 255 113 1 240 255 113 255 242 0 0 0 0 0000
./texmod undersea.tr2 a 12 1 114 1 240 255 116 1 240 255 116 255 242 0 0 0 0 0000
./texmod undersea.tr2 a 12 1 117 1 240 255 119 1 240 255 119 255 242 0 0 0 0 0000
./texmod zoo.tr2 a 14 255 31 255 255 255 31 1 240 1 0 255 255 0 0 0 0 0002
./texmod zoo.tr2 a 14 255 31 255 255 1 0 255 255 255 31 1 240 0 0 0 0 0002
./texmod zoo.tr2 a 14 1 32 1 240 255 47 1 240 255 47 255 255 1 32 255 255 0000
./texmod zoo.tr2 a 14 1 48 1 240 255 63 1 240 255 63 255 255 1 48 255 255 0000
./texmod zoo.tr2 a 14 1 64 1 240 255 79 1 240 255 79 255 255 1 64 255 255 0000
./texmod zoo.tr2 a 14 1 80 1 240 255 95 1 240 255 95 255 255 1 80 255 255 0000
./texmod zoo.tr2 a 14 1 96 1 240 255 98 1 240 255 98 255 242 0 0 0 0 0000
./texmod zoo.tr2 a 14 1 99 1 240 255 101 1 240 255 101 255 242 0 0 0 0 0000
./texmod zoo.tr2 a 14 1 102 1 240 255 104 1 240 255 104 255 242 0 0 0 0 0000
./texmod zoo.tr2 a 14 1 105 1 240 255 107 1 240 255 107 255 242 0 0 0 0 0000
./texmod zoo.tr2 a 14 1 108 1 240 255 110 1 240 255 110 255 242 0 0 0 0 0000
./texmod zoo.tr2 a 14 1 111 1 240 255 113 1 240 255 113 255 242 0 0 0 0 0000
./texmod zoo.tr2 a 14 1 114 1 240 255 116 1 240 255 116 255 242 0 0 0 0 0000
./texmod zoo.tr2 a 14 1 117 1 240 255 119 1 240 255 119 255 242 0 0 0 0 0000
./texmod slinc.tr2 a 11 255 31 255 255 255 31 1 240 1 0 255 255 0 0 0 0 0002
./texmod slinc.tr2 a 11 255 31 255 255 1 0 255 255 255 31 1 240 0 0 0 0 0002
./texmod slinc.tr2 a 11 1 32 1 240 255 47 1 240 255 47 255 255 1 32 255 255 0000
./texmod slinc.tr2 a 11 1 48 1 240 255 63 1 240 255 63 255 255 1 48 255 255 0000
./texmod slinc.tr2 a 11 1 64 1 240 255 79 1 240 255 79 255 255 1 64 255 255 0000
./texmod slinc.tr2 a 11 1 80 1 240 255 95 1 240 255 95 255 255 1 80 255 255 0000
./texmod slinc.tr2 a 11 1 96 1 240 255 98 1 240 255 98 255 242 0 0 0 0 0000
./texmod slinc.tr2 a 11 1 99 1 240 255 101 1 240 255 101 255 242 0 0 0 0 0000
./texmod slinc.tr2 a 11 1 102 1 240 255 104 1 240 255 104 255 242 0 0 0 0 0000
./texmod slinc.tr2 a 11 1 105 1 240 255 107 1 240 255 107 255 242 0 0 0 0 0000
./texmod slinc.tr2 a 11 1 108 1 240 255 110 1 240 255 110 255 242 0 0 0 0 0000
./texmod slinc.tr2 a 11 1 111 1 240 255 113 1 240 255 113 255 242 0 0 0 0 0000
./texmod slinc.tr2 a 11 1 114 1 240 255 116 1 240 255 116 255 242 0 0 0 0 0000
./texmod slinc.tr2 a 11 1 117 1 240 255 119 1 240 255 119 255 242 0 0 0 0 0000

# Replaces the animations to allow save crystals and controller mesh
for level in ${levels}; do
	./troi "${level}.tr2" i "./anims/${level}.dat"
done

# Adds savegame crystals
./trmod tr3pc scotland.tr2 "replaceitem(117,180,6,4608,4608,256,0,-1-1,3E00)" "replaceitem(48,180,79,8704,4608,512,0,-1-1,3E00)" "replaceitem(79,180,50,4608,3584,1280,0,-1,-1,3E00)" "replacefloordata(84,3,1:ceilingslant(2,0),trigger(trigger,0,,1F:object(118)))" "replacefloordata(84,4,1:trigger(trigger,0,,1F:object(118)))" "replacefloordata(84,5,1:ceilingslant(-2,0),trigger(trigger,0,,1F:object(118)))" "replacefloordata(36,5,1:ceilingportal(6),trifloor(\\,,1,0,1,2,0,0),trigger(trigger,0,,1F:object(21)))" "replacefloordata(36,5,2:ceilingportal(6),trigger(trigger,0,,1F:object(21)))" "replacefloordata(36,5,3:ceilingportal(6),trifloor(/,,4,0,0,0,5,1),trigger(trigger,0,,1F:object(21)))" "replacefloordata(65,10,1:monkeyswing,trigger(pickup(78),0,,1F:lookitem(77),camera(65,1536,2176,3424,4,0,once,0000)))"
./trmod tr3pc willsden.tr2 "additem(180,103,9728,1536,1280,0,-1,-1,3E00)" "additem(180,100,2560,1536,512,0,-1,-1,3E00)"
./trmod tr3pc chunnel.tr2 "additem(180,6,2560,5632,256,0,-1,-1,3E00)" "additem(180,91,1536,5632,1280,0,-1,-1,3E00)"


### LEVEL OPTIMISATION ###

# Rotates the diving bells to hide the part that doesn't exist in undersea.tr2
./trmod tr3pc undersea.tr2 "replaceitem(48,,,,,,16384,,,)"
./trmod tr3pc undersea.tr2 "replaceitem(50,,,,,,16384,,,)"

# Remove items to save size in undersea.tr2
./trmod tr3pc undersea.tr2 "replacefloordata(111,11,9:floorslant(-2,0),trigger(trigger,0,once,1F:soundtrack(14),camera(111,18944,3584,1408,7,0,once,0000),lookitem(143)))"
./trmod tr3pc undersea.tr2 "replacefloordata(111,10,9:trigger(trigger,0,,1F:object(144)))"
./trmod tr3pc undersea.tr2 "replacefloordata(2,3,3:ceilingslant(-2,0),trigger(pickup(3),0,,1F:camera(32,4608,3200,512,4,0,,0000),lookitem(131)))"
./trmod tr3pc undersea.tr2 "replacefloordata(10,1,4:trigger(switch(16),0,once,1F:object(15),object(14),soundtrack(54),object(131),lookitem(131),camera(32,4608,3200,512,4,0,once,0000),object(145)))"
./trmod tr3pc undersea.tr2 "removeitem(142)" "removeitem(141)" "removeitem(140)" "removeitem(132)" "removeitem(127)" "removeitem(126)" "removeitem(125)" "removeitem(87)" "removeitem(86)" "removeitem(85)" "removeitem(26)" "removeitem(25)" "removeitem(21)" "removeitem(11)" "removeitem(8)" "removeitem(6)"

# Optimises the world geometry
for level in ${levels}; do
	sh "./geom/${level}.sh"
done

# Optimises the mesh data
for level in ${levels}; do
	./tr3mesh "${level}.tr2" "./mesh/${level}.txt"
done

# Optimises the FloorData
for level in ${levels}; do
	fd="$(./trmod "tr3pc" "${level}.tr2" "cgetfloordata(*,*,*)" |
	      sed "s|^.*trmod tr3pc ${level}.tr2 ||g"               |
	      awk '{ print length, $0 }'                            |
	      sort -n -s -r                                         |
	      cut -d" " -f2-                                        |
	      tr '\n' ' '                                           |
	      fold -w "${shellbuffer}" -s                           |
	      sed "s|^|./trmod tr3pc ${level}.tr2 |g")"
	./trmod "tr3pc" "${level}.tr2" "removefloordata(*,*,*)"
	(
		IFS="$(printf '\n\v')"
		for fdline in ${fd}
		do
			(
				IFS=" "
				${fdline}
			)
		done
	)
done


### LEVEL CONVERSION ###

# Converts the levels
for level in ${levels}; do
	var="$(echo "${convparams}" |
	       tr   ';' '\n'        |
	       grep "${level}"      |
	       sed "s|.*:||g")"
	for version in ${versions}; do
		# Performs the actual level conversion
		./tr3pcpsx "${level}.tr2" "./out/${version}/${level}.psx" "./outsfx/${version}/${level}.sfx" "-v" $var "-${version}"
	done
done

# Copies output back into the project directory
cd "${prjdir}"
cp -r /tmp/levels/out .

