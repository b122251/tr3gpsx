# Shell script that unpacks this project directory.

#!/bin/sh
storedsum="$(cat ./tr3gpsx.lpq.sha256)"
calcsum="$(sha256 -q ./tr3gpsx.lpq)"
if [ "$storedsum" != "$calcsum" ];
then
	echo "Checksum of tr3gpsx.lpq does not match!"
else
	# Unpack the lpq file
	./lpaq1/bin/lpaq1 d ./tr3gpsx.lpq ./tr3gpsx.tar
	storedsum="$(cat ./tr3gpsx.tar.sha256)"
	calcsum="$(sha256 -q ./tr3gpsx.tar)"
	if [ "$storedsum" != "$calcsum" ];
	then
		echo "Checksum of tr3gpsx.tar does not match!"
	else
		tar -xf ./tr3gpsx.tar
	fi
fi
